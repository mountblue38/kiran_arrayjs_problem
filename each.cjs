function each(arr, cbFunction) {
    if (
        !Array.isArray(arr) ||
        arr == undefined ||
        typeof cbFunction != "function" ||
        cbFunction == undefined
    ) {
        return [];
    }
    for (let index = 0; index < arr.length; index++) {
        arr[index] = cbFunction(arr[index],index,arr);
    }
    return arr;
}

module.exports = each;
