function filter(arr, cbFunction) {
    if (
        !Array.isArray(arr) ||
        arr == undefined ||
        typeof cbFunction != "function" ||
        cbFunction == undefined
    ) {
        return [];
    }
    let resArr = [];
    for (let index = 0; index < arr.length; index++) {
        if (cbFunction(arr[index],index,arr) === true) {
            resArr.push(arr[index]);
        }
    }
    return resArr;
}

module.exports = filter;
