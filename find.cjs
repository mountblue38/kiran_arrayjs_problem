function find(arr, cbFunction) {
    if (
        !Array.isArray(arr) ||
        arr == undefined ||
        typeof cbFunction != "function" ||
        cbFunction == undefined
    ) {
        return [];
    }
    for (let index = 0; index < arr.length; index++) {
        if (cbFunction(arr[index], searchElement)) {
            return arr[index];
        }
    }
    return undefined;
}

module.exports = find;
