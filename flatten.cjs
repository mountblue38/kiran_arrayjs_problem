function flatten(arr, depth) {
    if (!Array.isArray(arr) || arr == undefined) {
        return [];
    }
    if (depth == undefined) {
        depth = 1;
    }
    let resFlatArr = [];
    for (let value of arr) {
        if (Array.isArray(value) && depth > 0) {
            resFlatArr = resFlatArr.concat(flatten(value, depth - 1));
        }
        else if(value == undefined){
            continue;
        } 
        else {
            resFlatArr.push(value);
        }
    }
    return resFlatArr;
}

module.exports = flatten;
