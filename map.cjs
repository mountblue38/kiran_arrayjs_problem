function map(arr, cbFunction) {
    if (
        !Array.isArray(arr) ||
        arr == undefined ||
        typeof cbFunction != "function" ||
        cbFunction == undefined
    ) {
        return [];
    }
    let resArr = [];
    for (let index = 0; index < arr.length; index++) {
        resArr.push(cbFunction(arr[index],index,arr));
    }
    return resArr;
}

module.exports = map;
