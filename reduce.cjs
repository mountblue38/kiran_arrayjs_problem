const items = [1, 2, 3, 4, 5, 5];
function reduce(arr, cbFunction, intialValue) {
    if (
        !Array.isArray(arr) ||
        arr == undefined ||
        typeof cbFunction != "function" ||
        cbFunction == undefined
    ) {
        return [];
    }
    let index = 0;
    if (intialValue == undefined) {
        intialValue = arr[0];
        index = 1;
    } else {
        index = 0;
    }
    for (index; index < arr.length; index++) {
        intialValue = cbFunction(intialValue, arr[index], index, arr);
    }

    return intialValue;
}

module.exports = reduce;
