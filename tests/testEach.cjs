const items = [1, 2, 3, 4, 5, 5];
const each = require("../each.cjs");

const result = each(items, function (itemValue) {
    return itemValue * 2;
});

console.log(result);
