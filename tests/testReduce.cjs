const items = [1, 2, 10, 3, 4, 5, 5];
const reduce = require("../reduce.cjs");
const cbFunction = (accumulator, itemVAlue) => {
    // return accumulator + itemVAlue;
    if (itemVAlue > accumulator) {
        accumulator = itemVAlue;
    }
    return accumulator;
};
const result = reduce(items, cbFunction, 0);

console.log(result);
